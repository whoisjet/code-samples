
# Improved form validation directive `pl-form-validate`

angular.module 'App'
  .directive 'plFormValidate', ['$timeout', ($timeout) ->
    restrict : 'A'
    require  : 'form'
    link     : (scope, form, attrs, formCtrl) ->
      # set first collection
      formElementCollection = []
      watchersCollection = []

      form
        .attr 'novalidate', ''
        .on 'change', ->
          formElementManager()
        .on 'submit', ->
          $timeout ->
            focusInput = form.find('.has-error .ng-invalid[ng-model]')[0]
            focusInput?.focus?()
          , 100

      messages = (error, value) ->
        switch error
          when "required"  then ""
          when "minlength" then "Value must be at least #{value} characters"
          when "maxlength" then "Value can be at most #{value} characters"
          when "url"       then "Invalid URL"
          when "email"     then "Invalid email"
          when "number"    then "Invalid number"
          when "confirm"   then "Value does not match"

      createWatcher = (input) ->
        ctrl = input.controller('ngModel')
        check = ->
          ctrl and ctrl.$invalid and (formCtrl.$submitted or not ctrl.$pristine)
        return scope.$watch check, (invalid) ->
          if ctrl
            message = input.attr('title') or (messages(err, input.attr("ng-#{err}")) for err of ctrl.$error)
              .filter _.identity
              .join '\n'
            input.parents('.pl-form-validate-parent,.form-group').first().toggleClass 'has-error', invalid
            input.parent().attr 'pl-form-validate-errors', message || null

      # send the new collection to the master collection
      toMasterCollection = (newCollection, collectionDiff = null)->
        # get the index of the new items in the new collection
        # push a new watch at index into the watchers collection
        if collectionDiff
          $.each collectionDiff, (i, item) ->
            index = $.inArray item, newCollection
            # insert new watch in watchers collection
            watchersCollection.splice index, 0, {
              index: index
              deregister: createWatcher( $(item) )
            }
        # if this is an empty slate, then make watchers
        else
          newCollection.each (i) ->
            # create watcher for this field
            watchersCollection.push {
              index: i
              deregister: createWatcher $(@)
            }
        # move new collection to master collection
        formElementCollection = newCollection
        # run a new digest
        if $.inArray(scope.$root.$$phase, ["$apply","$digest"]) is -1 then scope.$apply()

      getCollectionDifference = (oldList, newList) ->
        # I have to use jQuery functions to get the collection diff because jquery
        $.grep newList, (val) -> $.inArray(val, oldList) is -1

      formElementManager = ->
        newCollection = form.find('[ng-model][required]')
        # if there's a collection of existing form elements
        if formElementCollection.length
          collectionDiff = getCollectionDifference formElementCollection, newCollection
          # are there new fields to add to master collection?
          if collectionDiff.length then toMasterCollection newCollection, collectionDiff
          else
            # check if we're removing fields from the master collection
            collectionDiff = getCollectionDifference newCollection, formElementCollection
            if collectionDiff.length
              # remove each field in master collection
              $.each collectionDiff, (i, val) ->
                index = $.inArray val, formElementCollection
                # deregister watchers from watchers collection
                if watchersCollection[ index ]
                  watchersCollection[ index ].deregister()
                  # remove the watch after deregistration
                  watchersCollection.splice index, 1
              # set new collection to master collection
              formElementCollection = newCollection
        # if master collection is empty, override master collection
        else toMasterCollection newCollection

      # run the form element manager to gather all elements and their watchers
      formElementManager()
      # expose formElementManager
      formCtrl.refreshWatchers = formElementManager

  ]

angular.module 'App'
  .directive 'plFieldConfirm', [ ->
    restrict : 'A'
    require  : 'ngModel'
    scope    :
      confirmWith  : '=plFieldConfirm'
    link     : (scope, element, attrs, ngModelCtrl) ->
      ngModelCtrl.$validators.confirm = (modelValue) ->
        modelValue == scope.confirmWith

      scope.$watch "confirmWith", () ->
        ngModelCtrl.$validate()
  ]
