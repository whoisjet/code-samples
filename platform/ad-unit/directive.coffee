
angular.module 'App'
  .directive 'plAdUnit', [
    "CONFIG"
    (CONFIG) ->
      restrict    : 'A'
      transclude  : true
      templateUrl : CONFIG.assetUrl 'ad-unit/template.html'
      scope :
        ad_tag_url  : '=plAdUnit'
        format      : '=plAdUnitFormat'
      link  : (scope, el, attr) ->
        # setup
        scope.active_macros = []
        scope.pretty_url    = ''

        # Generate a list of macros filtered by format
        scope.CreativeMacrosByFormat = (format) ->
          result = {}
          unless format is null then for macro, macroData of scope.$root.ENUMS.CreativeMacro
            result[macro] = macroData if _.contains macroData["formats"], format
          # return new enum
          result

        # Update macros
        scope.update_macros = (ad_tag_url = '') ->
          # setup & reset
          scope.active_macros = []
          # grab all matching macros inside "{{}}"
          macrosList = ad_tag_url.match new RegExp(/\{\{.*?\}\}/g)
          # run through each macro found
          if macrosList then for macro, i in macrosList
            macroIndex = 0
            # start the index from the last active macro
            if scope.active_macros.length
              macroIndex = scope.active_macros[scope.active_macros.length - 1].index + 1
            # generate new macro data
            newMacro =
              id:           i
              name:         macro.replace(new RegExp(/\{\{|\}\}|\%/g), '')
              index:        ad_tag_url.indexOf macro, macroIndex
              url_encoded:  macro.indexOf("%") isnt -1
              length:       macro.length
            # push new macro to active macros array
            scope.active_macros.push newMacro
          # update the pretties
          scope.update_pretty_url()

        # update the name of the macro selected
        scope.update_macro = (macro) ->
          adFormUrl = scope.ad_tag_url or ''
          p1 = adFormUrl.slice(0, macro.index)
          p2 = "{{" + (if macro.url_encoded then "%" else "") + macro.name + "}}"
          p3 = adFormUrl.slice(macro.index + macro.length)
          scope.ad_tag_url = p1 + p2 + p3
          # update the macro length
          macro.length = p2.length
          # update active macros
          scope.update_macros scope.ad_tag_url

        # escape HTML
        scope.escapeHTML = (url) ->
          url.replace(new RegExp(/&/g), "&amp;")
            .replace(new RegExp(/</g), "&lt;")
            .replace(new RegExp(/>/g), "&gt;")
            .replace(new RegExp(/"/g), "&quot;")
            .replace(new RegExp(/'/g), "&#039;")

        # update pretty url
        scope.update_pretty_url = () ->
          adFormUrl = scope.ad_tag_url or ''
          # use batch replace to pretty up macros
          scope.pretty_url = scope.escapeHTML( adFormUrl )
            .replace new RegExp(/\{\{.*?\}\}/g), (macro) ->
              '<i class="creative-macro">' + macro + '</i>'

        # add an empty macro tag so that the update_macros will generate a new official macro
        scope.add_macro = ->
          scope.ad_tag_url = scope.ad_tag_url or ''
          scope.ad_tag_url += "{{}}"
          # update active macros
          scope.update_macros scope.ad_tag_url

        # remove macro
        scope.remove_macro = (macro) ->
          adFormUrl = scope.ad_tag_url or ''
          p = adFormUrl.slice(0, macro.index)
          s = adFormUrl.slice(macro.index + macro.length)
          scope.ad_tag_url = p + s
          # update active macros
          scope.update_macros scope.ad_tag_url

        # find ad base url and insert tag, update macros as well
        el.find('#ad_base_url').val scope.ad_tag_url
        scope.update_macros scope.ad_tag_url

        # watch format, if format changes, clear the active macros data
        scope.$watch 'format', (newVal, oldVal) ->
          if newVal isnt undefined and newVal isnt oldVal
            delete scope.active_macros
            scope.ad_tag_url = null

  ]
