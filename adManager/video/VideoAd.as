
package com.adaptivem.video {

  import flash.display.Sprite;
  import flash.events.Event;
  import flash.media.Video;
  import flash.media.SoundTransform;
  import flash.net.NetStream;
  import flash.net.NetConnection;
  import flash.events.AsyncErrorEvent;
  import flash.events.NetStatusEvent;
  import flash.utils.Timer;
  import flash.events.TimerEvent;

  import com.adaptivem.vars.Env;
  import com.adaptivem.vpaid.VPAIDEvent;
  import com.adaptivem.vpaid.IVPAID;
  import com.adaptivem.util.Log;

  import com.adaptivem.video.VideoUI;

  public class VideoAd extends Sprite implements IVPAID{

    private static var CLASS_NAME : String = "VideoAd"

    private var _media : String;
    private var _duration : int;
    private var _mediaAspectRatio : Number;
    private var ui : VideoUI;
    private var nc : NetConnection;
    private var stream : NetStream;
    private var video : Video;
    private var timer : Timer = new Timer(500);

    private var _firstQuartile : Boolean = false;
    private var _midpoint : Boolean = false;
    private var _thirdQuartile : Boolean = false;

    public function VideoAd(media : String, duration: int, width: int, height: int){
      try{
        _media = media;
        _duration = duration;
        _mediaAspectRatio = width / height;

        Log.info(CLASS_NAME, "media dimensions: " + width + "x" + height);
        Log.info(CLASS_NAME, "aspectRatio: " + _mediaAspectRatio);
        Log.info(CLASS_NAME, media);

        nc = new NetConnection();
        nc.connect(null);

        stream = new NetStream(nc);
        stream.checkPolicyFile = true;
        stream.client = this;
        stream.addEventListener(AsyncErrorEvent.ASYNC_ERROR, asyncErrorHandler);
        stream.addEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);

        // use env.width but calculate height
        video = new Video(Env.width, (Env.width / _mediaAspectRatio) );
        video.attachNetStream(stream);
        addChild(video);
        centerVideo();

        ui = new VideoUI(this);
        addChild(ui);

      }catch(error: Error){
        Log.info(CLASS_NAME, "error");
      }


      Log.info(CLASS_NAME, "constructor");
    }

    private function asyncErrorHandler(event : AsyncErrorEvent) : void {
      Log.info(CLASS_NAME, "asyncErrorHandler")
    }

    private function netStatusHandler(event : NetStatusEvent) : void {
      Log.info(CLASS_NAME, "netStatusHandler " + event.info.code)
      switch(event.info.code){
        case "NetStream.Play.Stop":
          timer.stop();
          dispatchEvent(new Event(VPAIDEvent.AdRemainingTimeChange));
          dispatchEvent(new Event(VPAIDEvent.AdVideoComplete));
          dispatchEvent(new Event(VPAIDEvent.AdStopped));
          break;
        case "NetStream.Play.Start":
          dispatchEvent(new Event(VPAIDEvent.AdStarted));
          dispatchEvent(new Event(VPAIDEvent.AdImpression));
          dispatchEvent(new Event(VPAIDEvent.AdVideoStart));
          dispatchEvent(new Event(VPAIDEvent.AdRemainingTimeChange));
          dispatchEvent(new Event(VPAIDEvent.AdDurationChange))
          break;
        case "NetStream.Pause.Notify":
          dispatchEvent(new Event(VPAIDEvent.AdPaused));
          break;
        case "NetStream.Unpause.Notify":
          dispatchEvent(new Event(VPAIDEvent.AdPlaying));
          break;
      }

    }

    public function get adLinear() : Boolean { return true; }

    public function get adExpanded() : Boolean { return false; }

    public function get adRemainingTime() : Number {
      // Log.info(CLASS_NAME, "duration: " + _duration);
      // Log.info(CLASS_NAME, "stream.time: " + stream.time);

      return _duration - stream.time;
    }

    public function get adVolume() : Number { return Env.volume; }

    public function set adVolume( v : Number ) : void {
      stream.soundTransform = new SoundTransform(v);
      // update env
      Env.volume = v;
      
      dispatchEvent(new Event(VPAIDEvent.AdVolumeChange));
    }

    // Methods
    public function handshakeVersion(playerVPAIDVersion:String):String { return "2.0" };

    public function initAd(width:Number, height:Number, viewMode:String, desiredBitrate:Number, creativeData:String = "", environmentVars:String = ""):void {
      Log.info(CLASS_NAME, "initAd");

      dispatchEvent(new Event(VPAIDEvent.AdLoaded));
    }

    public function resizeAd(w:Number, h:Number, viewMode:String):void {
      // remove video & ui
      removeChild(video);
      removeChild(ui);

      video = new Video(w, (w / _mediaAspectRatio) );
      video.attachNetStream(stream);
      addChild(video);
      centerVideo();

      addChild(ui);
      dispatchEvent(new Event(VPAIDEvent.AdSizeChange));
    };

    private function centerVideo() : void {
      video.y = (Env.height / 2) - (video.height / 2);
      Log.info(CLASS_NAME, "centerVideo() X: " + video.x + ", Y:" + video.y);
    }

    public function startAd():void {
      try{
        stream.play(_media);

        timer.addEventListener(TimerEvent.TIMER, tickHandler)
        timer.start();
      }
      catch(error: Error){
        Log.info(CLASS_NAME, error.message);
      }
    };

    private function tickHandler(event: TimerEvent): void{

      var ratio : Number = stream.time / _duration;

      switch(true){
        case ratio > 0.75:
          if(!_thirdQuartile){
            _thirdQuartile = true;
            dispatchEvent(new Event(VPAIDEvent.AdVideoFirstQuartile));
          }
        case ratio > 0.5:
          if(!_midpoint){
            _midpoint = true;
            dispatchEvent(new Event(VPAIDEvent.AdVideoMidpoint));
          }
        case ratio > 0.25:
          if(!_firstQuartile){
            _firstQuartile = true;
            dispatchEvent(new Event(VPAIDEvent.AdVideoFirstQuartile));
          }
      }


      dispatchEvent(new Event(VPAIDEvent.AdRemainingTimeChange));
    }

    public function stopAd():void {};

    public function pauseAd():void {
      timer.stop();
      stream.pause();
    };

    public function resumeAd():void {
      if(stream.time < _duration){
        timer.start();
        stream.resume();
      }
    };

    public function expandAd():void {};

    public function collapseAd():void {};

    // 2.0 Methods
    public function get adWidth():Number {return Env.width};

    public function get adHeight():Number { return Env.height};

    public function get adIcons():Boolean { return false };

    public function get adSkippableState():Boolean { return false };

    public function get adDuration():Number { return _duration };

    public function get adCompanions():String { return "" };

    public function skipAd():void { };


  }

}