
package com.adaptivem.video {

  import flash.display.Sprite;
  import flash.display.Graphics;
  import flash.display.MovieClip;
  import flash.display.LineScaleMode;
  import flash.events.Event;
  import flash.events.MouseEvent;
  import flash.ui.Mouse;
  import flash.text.TextField;
  import flash.text.TextFormat;
  import flash.text.TextFormatAlign;

  import com.adaptivem.vpaid.VPAIDEvent;
  import com.adaptivem.util.Log;

  import com.adaptivem.vars.Env;

  public class VideoUI extends Sprite {

    [Embed(source="fonts/ui4-am.ttf",
      fontName="ui4-am",
      mimeType="application/x-font",
      fontWeight="normal",
      fontStyle="normal",
      advancedAntiAliasing="true",
      embedAsCFF="false")]
    private var myEmbeddedFont : Class;

    private static var CLASS_NAME : String = "VideoUI"

    private const PLAY_TEXT : String = "l";
    private const PAUSE_TEXT : String = "a";
    private const MUTE_TEXT : String = "u";
    private const UNMUTE_TEXT : String = "x";

    private var VPAIDEventList : Array = [
      VPAIDEvent.AdStarted,
      VPAIDEvent.AdPlaying,
      VPAIDEvent.AdPaused,
      VPAIDEvent.AdStopped,
      VPAIDEvent.AdVolumeChange,
      VPAIDEvent.AdSizeChange,
    ];

    private var _adReference : *;

    private var textFormat : TextFormat = new TextFormat();
    private var _buttonPlay : TextField = new TextField();
    private var _buttonMute : TextField = new TextField();
    private var _controlBar : MovieClip;

    private var hasStarted : Boolean = false;
    private var isPaused : Boolean = true;
    private var isMuted : Boolean = false;
    private var storedVolume : Number = 0.85;

    // private var borderRadius : Number = 0.5;
    private var controlBarHeight : Number = 34;
    private var fontSize: Number = 24;
    private var margin : Number = 8;
    private var padding : Number = 100; // TODO: Incorporate padding, for ease of button press
    private var opacity : Number = 0.6;
    private var borderWidth : Number = 0;
    private var bgColor : uint = 0x000000;
    private var txtColor : uint = 0xffffff;

    public function VideoUI(adReference : *){
      try{
        _adReference = adReference;

        formatText();

        createPlay();
        createMute()
        adjustControlBarHeight();
        createControlBar();

        // create event listeners
        for(var i : int = 0; i < VPAIDEventList.length; i++){
          var eventType : String = VPAIDEventList[i];
          _adReference.addEventListener(eventType, VPAIDEventHandler);
        }

        // add elements
        addChild( _controlBar );
        addChild( _buttonPlay );
        addChild( _buttonMute );

      } catch(error : Error) {
        Log.info(CLASS_NAME, error.getStackTrace());
      }
      Log.info(CLASS_NAME, "VideoAd()");
    }

    private function VPAIDEventHandler(event : Event) : void {
      switch(event.type){
        case VPAIDEvent.AdStarted:
          hasStarted = true;
        case VPAIDEvent.AdPlaying:
          setToPlayState();
          _buttonPlay.setTextFormat(textFormat);
        break;

        case VPAIDEvent.AdPaused:
        case VPAIDEvent.AdStopped:
          setToPausedState();
          _buttonPlay.setTextFormat(textFormat);
        break;

        case VPAIDEvent.AdVolumeChange:
          Log.info(CLASS_NAME, "_adReference.adVolume " + _adReference.adVolume);
          if(_adReference.adVolume == 0 || NaN) {
            setToMuteState();
          } else {
            storedVolume = _adReference.adVolume;
            setToUnmuteState();
          }
          _buttonMute.setTextFormat(textFormat);
        break;

        case VPAIDEvent.AdSizeChange:
          resizeUI();
        break;
      }
    }

    private function formatText() : void {
      textFormat.font = "ui4-am";
      textFormat.size = fontSize;
      textFormat.align = TextFormatAlign.CENTER;
    }

    // UI Positioning
    public function resizeUI() : void {
      Log.info(CLASS_NAME, "resizeUI() w: " + _adReference.adWidth + ", h: " + _adReference.adHeight);
      repositionButton( _buttonPlay, margin );
      repositionButton( _buttonMute, (_adReference.adWidth - (_buttonMute.width + margin)) );
      _controlBar.width = _adReference.adWidth;
      _controlBar.y = _adReference.adHeight - controlBarHeight;
    }

    private function repositionButton(buttonIcon : TextField, posX : Number) : void {
      Log.info(CLASS_NAME, "repositionButton()");
      buttonIcon.x = posX;
      Log.info(CLASS_NAME, "_adReference " + _adReference.adHeight);
      Log.info(CLASS_NAME, "Env.height " + Env.height);
      buttonIcon.y = (_adReference.adHeight ? _adReference.adHeight : Env.height) - (buttonIcon.height + margin);
    }

    // UI Creation
    private function setButtonParameters(buttonIcon : TextField, buttonText : String) : void {
      Log.info(CLASS_NAME, "setButtonParameters() " + buttonText);
      buttonIcon.embedFonts = true;
      buttonIcon.text = buttonText;
      buttonIcon.textColor = txtColor;
      buttonIcon.background = false;
      buttonIcon.multiline = false;
      buttonIcon.wordWrap = false;
      buttonIcon.setTextFormat(textFormat);
      // dimensions
      buttonIcon.height = buttonIcon.textHeight + 6;
      buttonIcon.width = buttonIcon.height;
    }

    private function bindUIListeners(buttonIcon : TextField, type : String) : void {
      Log.info(CLASS_NAME, "bindUIListeners() " + type);
      buttonIcon.addEventListener(MouseEvent.CLICK, (type == "playPause") ? playPause : mute);
      buttonIcon.addEventListener(MouseEvent.MOUSE_OVER, cursorPointer);
      buttonIcon.addEventListener(MouseEvent.MOUSE_OUT, cursorDefault);
    }

    private function createPlay() : void {
      Log.info(CLASS_NAME, "createPlayUI()");
      setButtonParameters(_buttonPlay, PLAY_TEXT);
      repositionButton( _buttonPlay, margin );
      bindUIListeners(_buttonPlay, "playPause");
    }

    private function createMute() : void {
      Log.info(CLASS_NAME, "createMuteUI()");
      setButtonParameters(_buttonMute, UNMUTE_TEXT);
      repositionButton( _buttonMute, (_adReference.adWidth - (_buttonMute.width + margin)) );
      bindUIListeners(_buttonMute, "mute");
    }

    private function adjustControlBarHeight() : void {
      controlBarHeight = _buttonPlay.height + (margin * 2);
      Log.info(CLASS_NAME, "new controlBarHeight " + controlBarHeight);
    }

    private function createControlBar() : void {
      Log.info(CLASS_NAME, "createControlBar()");
      _controlBar = new MovieClip();
      _controlBar.graphics.lineStyle(borderWidth, bgColor, opacity);
      _controlBar.graphics.beginFill(bgColor, opacity);
      _controlBar.graphics.drawRect(0, 0, _adReference.adWidth, controlBarHeight);
      _controlBar.graphics.endFill();
      // move bar down
      
      _adReference.addEventListener(MouseEvent.ROLL_OVER, manageMouseOver, false, 0, true);
      _adReference.addEventListener(MouseEvent.ROLL_OUT, manageMouseOut, false, 0, true);
      
      _controlBar.y = _adReference.adHeight - controlBarHeight;
    }
    
    private function manageMouseOver(event:MouseEvent):void{
        Log.info(CLASS_NAME, "mouseover");
        this.visible = true;
      }
       
    private function manageMouseOut(event:MouseEvent):void{
      Log.info(CLASS_NAME, "mouseout");
      this.visible = false;
    }
    

    // MISC
    private function cursorPointer(event : MouseEvent) : void {
      Mouse.cursor = "button";
    }
    private function cursorDefault(event : MouseEvent) : void {
      Mouse.cursor = "auto";
    }

    // States
    private function setToPlayState() : void {
      Log.info(CLASS_NAME, "setToPlayState()");
      _buttonPlay.text = PAUSE_TEXT;
      isPaused = false;
    }

    private function setToPausedState() : void {
      Log.info(CLASS_NAME, "setToPausedState()");
      _buttonPlay.text = PLAY_TEXT;
      isPaused = true;
    }

    private function setToMuteState() : void {
      Log.info(CLASS_NAME, "setToMuteState()");
      _buttonMute.text = MUTE_TEXT;
      isMuted = true;
    }

    private function setToUnmuteState() : void {
      Log.info(CLASS_NAME, "setToUnmuteState()");
      _buttonMute.text = UNMUTE_TEXT;
      isMuted = false;
    }
    
    public function updateStates() : void {
      if(_adReference.adVolume == 0 || NaN) {
        setToMuteState();
      } else {
        storedVolume = _adReference.adVolume;
        setToUnmuteState();
      }
      _buttonMute.setTextFormat(textFormat);
    }

    // Methods
    private function playPause(event : MouseEvent = null) : void {
      Log.info(CLASS_NAME, "playPause()");
      if (isPaused && !hasStarted) {
        _adReference.startAd();
        hasStarted = true;
      } else if (isPaused && hasStarted) {
        _adReference.resumeAd();
      } else {
        _adReference.pauseAd();
      }
    }

    private function mute(event : MouseEvent = null) : void {
      Log.info(CLASS_NAME, "mute() " + isMuted);
      // var userInit:Boolean = e != null;
      if (isMuted) {
        _adReference.adVolume = storedVolume;
        storedVolume = 0;
        setToUnmuteState()
      } else {
        storedVolume = _adReference.adVolume || 0.85;
        _adReference.adVolume = 0;
        setToMuteState()
      }
    }

  }

}