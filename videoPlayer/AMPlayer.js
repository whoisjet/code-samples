'use strict'

/*global Log*/
/*global Properties*/
/*global Playlist*/
/*global MediaItem*/
/*global VASTParser*/
/*global PlayerContainer*/
/*global VPAIDJSContainer*/
/*global VPAIDPlugin*/
/*global VPAIDJSPlugin*/
/*global WaterfallPlugin*/
/*global SWFLoader*/
/*global player*/
/*global VideoAdLoader*/
/*global swfobject*/
/*global WRAPPER_LIMIT*/
/*global Tracking*/
/*global com*/
/*global debugMode*/
/*global PropertiesWindow*/
/*global Client*/
/*global Css*/
/*global Misc*/
/*global GoogleIMA*/
/*global script*/

const CLASS_NAME = "AMPlayer";

class AMPlayer {

  constructor (token, uid, userDefined) {
    // Browser client data

    // Fire tracking event
    Tracking.firePlayerEvent("initPlayer", this._client)

    userDefined = userDefined ? userDefined : {};
    this._VPAIDJS = userDefined.vpaidjs;

    // insert css file to document
    this.__loadCSS(userDefined.testing);

    Log.info(this.constructor.name,"constructor("+token+","+uid+")");

    // Basic Setup
    this._token = token;
    this._uid = uid;

    this._script = userDefined.videoSlot;

    if(script && !this._VPAIDJS){
      this._script = script;
    }
    this._currentPlaylist = 0;

    Tracking.setupVPAIDEvents(this._script)

    if(this.isMobile){
      userDefined.adType = "js"
    }

    // Create player container before Agora call is returned (apparent speed)
    if(this._VPAIDJS){
      this._properties = userDefined;
      this._playerContainer = new VPAIDJSContainer(this._uid, userDefined.width, userDefined.height, this._script, userDefined.slot)
      setTimeout(this.__propertiesLoadComplete, 0);
    }
    else{
      // Load properties
      this._properties = new Properties(this._token, userDefined, this.__propertiesLoadComplete);
      this._playerContainer = new PlayerContainer(this._uid, this._properties.width, this._properties.height, this._script);
    }
    Tracking.setupMediaEvents(this._playerContainer._video)

    // reset ad
  }

  __propertiesLoadComplete(){
    // debugmode

    // Update UI
    Log.info("AMPlayer", "propertiesLoadComplete()")
    // Volume adjustments

    player._makeRequests()
  }

  _makeRequests(){
    // enter ad mode
    if(this._properties.ads != "null")
      this._loadAds()
    if(!this._VPAIDJS)
      this._checkPlaylist();
    else
      Log.info("AMPlayer-VPAIDJS", "Skipping playlist loading");
  }

  _loadAds(){

    var w = window;
    while(w.location != w.parent.location){
      w = w.parent;
    }

    Log.info(CLASS_NAME, "referrer: " + w.location.href);

    this._properties.ads = Misc.addQueryParams( this._properties.ads, {
      width: this._properties.width,
      height: this._properties.height,
      VPAID: this._properties.adType,

      referrer: w.location.href
    });

    if(!this._VPAIDJS) /* Normal Player flow */ {
      this._parser = new VASTParser(this._properties.ads, WRAPPER_LIMIT);
    }
    else{
      this._waterfalling = true;
      this._parser = new WaterfallPlugin(this._properties.ads);
    }

    this._parser.load( this.__inlineCallback.bind(this),function errorCallback(message){
      Log.info(CLASS_NAME, "there has been a parser error: " + message)
      player._waterfalling = false;
      player.__adError(message);
    }
    )
  }

  __inlineCallback(){
    Log.info("AMPlayer", "inlineCallback")
    if(player._parser.valid){
        // If the ad is google IMA, run it through that flow instead
        if (player._parser.adSystem == GoogleIMA.adSystemText) {
          Log.info("AMPlayer", "Google IMA Ad Detected.")
          player._ad = new GoogleIMA( this._properties.ads, function GoogleIMAReady() {
            player._ad.initIMA();
          });

        // All non-IMA ads will run normally
        } else {
          Log.info("AMPlayer", "Running ads normally.")
          var mediaType = player._parser.mediaType;
          Log.info("AMPlayer", "media type: " + mediaType)
          if(mediaType == VASTParser.MEDIA_TYPE_MP4){
            // Load Video Player
            player._adLoadedTimeout();
            player.loadVideoAd(player._parser.mediaFile)
          }
          else if((mediaType == VASTParser.MEDIA_TYPE_FLV) || (mediaType == VASTParser.MEDIA_TYPE_SWF)){
            // Load SWF Player
            player._adLoadedTimeout();
            player.loadSWF(player._parser.mediaFile)
          }
          else if(mediaType == VASTParser.MEDIA_TYPE_JS){
            // Load JS Ad
            var prod_waterfall = player._parser.mediaFile.textContent.trim() == "CDNURL/AdManager";
            var testing_waterfall = player._parser.mediaFile.textContent.trim() == "DEVURL/AdManager";

            if(prod_waterfall || testing_waterfall){
              Log.info("AMPlayer","Change of plans, we're handling this ad locally in our own waterfall");
              player._redirectToWaterfall();
            }
            else{
              Log.info("AMPlayer", "Third party VPAIDJS Ad");
              player._adLoadedTimeout();
              player.loadJSAd(player._parser.mediaFile)
            }
          }
          else{
            // Unsupported File type
            player.__adError("Unsupported MediaType");
          }
        }
      }
      else{
        player.__adError("Invalid VAST: " + player._parser.errorMessage);
      }
  }

  _redirectToWaterfall(){
    Log.info(CLASS_NAME, "redirectToWaterfall")
    this._waterfalling = true;
    var adParameters = JSON.parse(player._parser.aggregateElements("AdParameters")[0].textContent)
    if(player._properties.adParameters != null)
      adParameters = JSON.parse(player._properties.adParameters);

    this._parser = new WaterfallPlugin(adParameters);

    this._parser.load(this.__inlineCallback.bind(this),function errorCallback(message){
      Log.info(CLASS_NAME, "there has been a waterfall parser error: " + message)
      player._waterfalling = false;
      player.__adError(message);
    }
    )
  }

  _checkPlaylist(){
    if(this._playlist == null)
      this._loadPlaylist()
    else{
      // Begin Phase-4
      this.__loadPlaylistComplete()
    }
  }

  _loadPlaylist(){
    Log.info(CLASS_NAME, "loadPlaylist")
    var playlistURI = this._properties.playlists[this._currentPlaylist];

    this._playlist = new Playlist(playlistURI, this._properties.playlistCycles);
    this._playlist.onSuccess = function(data){
      player._playlistId = data.id
      player.__loadPlaylistComplete()
    }

    this._playlistLoader = this._playlist.load();
  }

  __loadPlaylistComplete(){
    if(this._properties.ads == "null"){
      // exit ad mode
    }
    else
      // trigger waiting module

    var posterImage;
    if(this._properties.poster == null){
      // Sort method did not work because iOS devices javascript sort direction is reversed from everyone else.
      // http://stackoverflow.com/questions/1380089/javascript-sort-with-function-not-working-on-iphone
      // http://stackoverflow.com/questions/34646722/array-sort-get-different-result-in-ios
      // var posterImage = this._playlist.mediaItems[0].thumbnails.sort(function(a,b){return a.width < b.width;})[0].url
      var largestWidth = 0;

      for(var i = 0; i < this._playlist.mediaItems[0].thumbnails.length; i++){
        if(this._playlist.mediaItems[0].thumbnails[i].width > largestWidth){
          largestWidth = this._playlist.mediaItems[0].thumbnails[i].width
          posterImage = this._playlist.mediaItems[0].thumbnails[i].url
        }
      }
      this._playerContainer.setPosterImage(posterImage);
    }
    else{
      this._playerContainer.setPosterImage(this._properties.poster);
    }

    // hide next button in this check

    if(this._playerContainer.thumbs == null && this._properties.showThumbs && !this._properties.disableThumbs){
      Log.info(CLASS_NAME, "constructThumbs")
      this._playerContainer.constructThumbs(this._playlist, this._properties.width + "px", function onclick(i){
        if(!player._adMode){
          // exit content phase
        }
      });
    }
    Log.info(CLASS_NAME, "complete playlistLoadHandler")

  }

  _enterAdMode(){
    Tracking.firePlayerEvent("initAd")
    this._adMode = true;
    this._playerContainer.canSeek = false;
  }

  _waitingModule(whoFinished){
    Log.info(CLASS_NAME,"_waitingModule: " + whoFinished)

    if(whoFinished == "playlist")
      this._playlistLoaded = true;
    else if(whoFinished == "adStopped"){
      this._adStopped = true;
      this._skipping = false;
    }

    if(this._playlistLoaded && this._adStopped){
      // exit admode
    }

  }

  _resetWaitingModule(){
    this._adStopped = false;
    this._playlistLoaded = false;
  }


  _exitAdMode(){
    Tracking.firePlayerEvent("leaveAdState");

    this._playerContainer.showPreloader();

    if(this._ad != undefined && this._ad.destroy != undefined){
      this._ad.destroy();
    }
    Log.info(CLASS_NAME, "destroy completed");
    this._adMode = false;
    this._resetAd();
    Log.info(CLASS_NAME, "resetAd completed");
    this._resetWaitingModule();
    Log.info(CLASS_NAME, "resetWaitingModule completed");
    this._beginContentPhase();
    Log.info("AMPlayer", "_exitAdMode Completed")
  }

  _beginContentPhase(){
    this._playerContainer.canSeek = true;
    this._loadNextMediaItem();
  }

  _loadNextMediaItem(){
    if(this._playlist.hasNext()){

      var _mediaItemURI = this._playlist.nextItem.replace(".json",".jsonp");

      // hit API to request new data
      this._mediaItem = new MediaItem(_mediaItemURI);
      this._mediaItem.onSuccess = function(data){
        player._playMediaItem(data);
        player._mediaItemId = data.id
      }

      this._mediaItem.load();
    }
  }

  _playMediaItem(mediaItem){
    Log.info(CLASS_NAME, "playMediaItem")
    // Parse out the asset
    var asset = mediaItem.contents[0];
    this._appendAsset(asset)

    if(this._adMode){
      this._adStarted = true;
    }
    this._source.addEventListener("error", function(){
      player.__mediaError()
    })

    if(this._client.isMobile){
      Log.info(CLASS_NAME, "I'm a mobile player!");
    }

    this._playerContainer._video.addEventListener("loadeddata", function adLoadedListener(){
      Log.info(CLASS_NAME, "loadeddata");

      if(!player._adMode){
        Tracking.firePlayerEvent("contentImpression")
        if (player._properties.autoPlay ) {
          Log.info(CLASS_NAME, "autoplay")
        }
      }
      else
        VPAIDPlugin.fire("AdImpression");
    })

    this.load();
  }

  _appendAsset(asset){
    // Remove previous source
    // clear any css from left over ad or content
    // If there's a video source remove it
    // Append new source
  }

  __mediaError(){
    if(this._adMode)
      this._adFailed = true
    else
      this._exitContentPhase()
  }

  _exitContentPhase(){
    this._contentLoaded = false;
    if(this._playlist.hasNext()){
      this._makeRequests()
    }
    else if(this._playlist.hasRemainingCycles()){
      // Cycle the playlist
      this._playlist.cycle();
      this._makeRequests();
    }
    else if(this._properties.hasNextPlaylist(this._currentPlaylist)){
      // Go to next playlist
      this._currentPlaylist += 1;
      delete this._playlist;
      // Remove Thumbs
      this._makeRequests();
    }
    else{
      Log.info(CLASS_NAME, "no play cycles remaining")
    }
  }







  /* ########## */
  /* Player API */
  /* ########## */

  api(){
    // return player api
  }

  play () {
    this._properties.autoPlay = true
    if(this._adMode){
      if(this._adStarted){
        this._ad.resumeAd();
        Log.info(CLASS_NAME, "resumeAd()");
      }
      else if(this._adLoaded){
        this._ad.startAd();
        Log.info(CLASS_NAME, "startAd() w timeout : " + player._properties.adTimeout + "ms");

        setTimeout(function adVideoStartTimeout(){

          // If the ad hasn't declared AdVideoStart after 4 seconds, timeout
          Log.info(CLASS_NAME, "adStarted: " + (player._adStarted == true));

          if(!player._adStarted){
            player.__adError("Ad Timeout");
          }


        }, player._properties.adTimeout)
      }
      else
        Log.error(CLASS_NAME, "ad has not initialized");
    }
    else{
      Log.info(CLASS_NAME, "content mode: play")
      try{
        this._playerContainer._video.play();
        Log.info(CLASS_NAME, "play() call successful");
      }
      catch(error){
        Log.error(CLASS_NAME, "error during play()")
      }
    }
  }

  setPlaybackRate(rate){
    Log.info(CLASS_NAME, "setPlaybackRate: ", rate);
    // set playback rate
  }

  get duration(){
    if(this._playerContainer._video == undefined)
      return -1;

    if(!this._adMode) /*Content Phase*/
      return // video duration
    else /*Ad Phase*/
      return // ad duration
  }

  seek(time){
    this._playerContainer._video.currentTime = time;
  }

  get currentTime(){
    return // current time
  }

  get volume(){
    if(this._playerContainer._video == undefined)
      return -1
    // Always set this volume
    return // video volume
  }

  set volume(v){
    Log.info("AMPlayer","new volume: "+v)
    // set volume
  }

  load(){
    this._playerContainer._video.load();
  }

  pause(){
    this._properties.autoPlay = false
    Log.info(CLASS_NAME, "pause()");
    if(this._adMode){
      this._ad.pauseAd();
      this._adPaused = true;
    }
    else{
      this._playerContainer._video.pause();
    }
  }

  mute(){
    Log.info(CLASS_NAME, "mute()");
    this._playerContainer.mute();
  }

  isMuted(){
    return this._playerContainer._ui.isMuted;
  }

  isPaused(){
    if(this._adMode)
      return this._adPaused;
    else
      return this._playerContainer._video.paused;
  }

  autoNext(){
    if(this._properties.autoNext)
      this.next()
  }

  next(){
    if(!this._adMode && this._playlist.hasNext()){
      player._skipping = true;
      // exit content phase
    }
  }

  destroy() {
    this._playerContainer.remove();
    this._destroyed = true;
  }

  resize(w,h){
    this._playerContainer.resize(w,h)
    if(this._adMode && !this.fullscreen){
      console.log("resizeAd("+w+","+h+")")

      var viewMode = "normal"
      this._ad.resizeAd(w,h,viewMode);
    }
  }

  get width() {
    return this._playerContainer.width;
  }

  get height() {
    return this._playerContainer.height;
  }

  showLog(){
    Log.show();
  }




  /* ####################### */
  /* Loader methods */
  /* ####################### */

  _adLoadedTimeout(){
    Log.info("AMPlayer", "setting a new timeout for adLoaded : " + player._properties.adTimeout + "ms")
    if(this.adLoadedTimeout)
      clearTimeout(this.adLoadedTimeout);
    this.adLoadedTimeout = setTimeout( function adLoadedTimeout(){

      // If the ad hasn't declared AdVideoStart after 4 seconds, timeout

    }, player._properties.adTimeout );
  }

  loadSWF(mediaFile){
    Log.info("AMPlayer", "loadSWF()");
    if(this._client.isMobile){
      Log.info("AMPlayer", "can't embed swf on mobile, skip it!");
      this.__adError("can't embed SWF on mobile");
    }
    else{
      this._adMediaFile = mediaFile;

      if(this._flashContainer == undefined){
        // insert flash container
      }

      this._flashTarget = document.createElement("div");
      this._flashContainer.appendChild(this._flashTarget);

      var loader = new SWFLoader();
      loader.load();
    }
  }

  swfLoaded(){
    Log.info("AMPlayer", "swfLoaded()");
    if(this._adMode){
      var mediaFileURI = this._adMediaFile.textContent
      var width = this.width;
      var height = this.height;
      var viewMode = "normal";
      var bitrate = 400;
      var creativeData = "";
      if(player._parser.aggregateElements("AdParameters").length > 0){
       creativeData= player._parser.aggregateElements("AdParameters")[0].textContent;
       if(player._properties.adParameters != null)
        creativeData = player._properties.adParameters;
      }
      creativeData = JSON.parse(creativeData);
      creativeData.volume = this._volume;
      creativeData.debugMode = debugMode;

      Log.info("AMPlayer", "initAd");
      this._ad.initAd(width,height,viewMode,bitrate,JSON.stringify(creativeData),JSON.stringify({
        // mediaFileURI: "RESOURCEURL/client.swf"
      }
      ));
    }
    else{
      Log.info("swfLoaded but no longer in ad mode")
    }
  }


  loadVideoAd(mediaFile){
    var clickThroughElements = this._parser.aggregateElements("ClickThrough");
    var clickThrough;
    if(clickThroughElements.length){
      clickThrough = clickThroughElements[0].textContent
    }

    this._ad = new VideoAdLoader(this._playerContainer, mediaFile , clickThrough);

    var videoAdEnded = function(){
      if(player._adMode){
        // ad stopped
      }
    }

    this._ad.addEventListener("ended", videoAdEnded);

    var mediaItem = {
      contents  : [ { url: mediaFile.textContent } ]
    }

    this._playMediaItem(mediaItem)
  }

  loadJSAd(mediaFile){
    this._ad = new VPAIDJSPlugin(mediaFile, function adLoaded(){
      player._adLoaded = true;
      VPAIDPlugin.fire("AdLoaded");
    }, function adStopped(){
      // ad stopped
    });
  }




  /* ####################### */
  /* Helper methods */
  /* ####################### */

  vpaidListener(eventType, data){
    Log.info("AMPlayer", "VPAID." + eventType)
    VPAIDPlugin.eventHandler(eventType, data);
  }

  setAd(ad){
    this._ad = ad;
  }

  __adError(message){
    Log.info("AMPlayer", "AdError")
    if(!this._waterfalling){
      this._adFailed = true;
      Log.info(CLASS_NAME,"adError : "+message+" : continue with content : " + this._parser._vastURI);
      VPAIDPlugin.fire("AdError");
    // ad stopped
    }
    else{
      Log.info("AMPlayer", "showNextValidAd()");
      this._resetAd();
      this._adMode = true;
      Tracking.firePlayerEvent("waterfallAdError" + player._parser.currentIndex)
      this._parser.showNextValidAd();
    }
  }


  _resetAd(){
    // reset ad variables
    // reset ad and content flags

    if(this.adLoadedTimeout){
      clearTimeout(this.adLoadedTimeout);
    }
    Log.info(CLASS_NAME, "adLoadedTimeout cleared");


    if(this._flashContainer != undefined){
      try{
        swfobject.removeSWF(this._ad.id);
        Log.info(CLASS_NAME, "swf removed");
      }catch(error){
        Log.info(CLASS_NAME, "swf failed to remove");
      }
    }
  }

  __loadCSS(testing){
    Log.info(CLASS_NAME, "loadCSS")
    if(this._VPAIDJS || (!this._VPAIDJS && com && com.adaptivem && !com.adaptivem.theia.cssLoaded)){
      if(!this._VPAIDJS)
        com.adaptivem.theia.cssLoaded = true;

      var cssFile = document.createElement("link");
      cssFile.setAttribute("rel", "stylesheet");
      // This needs to change when not in testing.
      cssFile.href = "CDNURL/main";

      if(testing)
        cssFile.href = "DEVURL/main"

      document.head.appendChild( cssFile );
    }
  }

}

